This is a step-by-step implementation of the catch a goblin game
demo01: Start with explaining the first implementation of how to render the game’s background in canvas. Walk them through, explain, have them see it!



demo2: Now render a second image of the hero in the background. Walk them through, explain, have them see it!

demo3: Learn to use listeners and handlers make the hero move by drawing image to a new position within the canvas. Walk them through, explain, have them see it!

demo4: Render a third monster image. Walk them through, explain, have them see it!

demo5: Implement the collision detection and handle it. Walk them through, explain, have them see it!

demo6: Implement a scoring mechanism and report to player. Walk them through, explain, have them see it!

demo7: Implement a bounding box in the game world. Walk them through, explain, have them see it!

Game complete.  You should built a simple graphics based game that runs in any browser.
Show them how to deploy on heroku and play it through web.

Additional notes:
I’ve included additional images so that you can show how you can switch between assets and how you can explicitly draw an image to a certain size passing height and width parameters. So take the time to show them how they can easily modify this game to be there own.

Extra Credit:
Expand the game and add your own modifications to it now!

original source for this tutorial:
http://www.lostdecadegames.com/how-to-make-a-simple-html5-canvas-game/