// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

/************************************************************
 * Explanation:  
 * Open the game00.html file in browser and
 * inspect the page to see that this javascript code has 
 * added a new canvas element with the specified dimensions 
 * into the active html page.
 *********************************************************** /
