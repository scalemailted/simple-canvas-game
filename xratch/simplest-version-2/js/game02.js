// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
var bgImage = new Image();
bgImage.src = "images/background.png";

// #Hero image
var heroImage = new Image();
heroImage.src = "images/hero.png";

// #Game objects
var hero = { };

//#hero pos
hero.x = canvas.width / 2;
hero.y = canvas.height / 2;


var render = function () 
{ 
	ctx.drawImage(bgImage, 0, 0); 
	ctx.drawImage(heroImage, hero.x, hero.y); //#render hero

};


// The main game loop
var main = function () { render(); };


// Let's play this game!
window.setInterval(main, 1); // Execute as fast as possible
