// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
var bgImage = new Image();
bgImage.src = "images/background.png";

// Hero image
var heroImage = new Image();
heroImage.src = "images/hero.png";

// #Monster image
var monsterImage = new Image();
monsterImage.src = "images/monster.png";

// Game objects
var hero = { speed: 1 /*movement in pixels per second*/ };
var monster = {};


//hero pos
hero.x = canvas.width / 2;
hero.y = canvas.height / 2;

// #Throw the monster somewhere on the screen randomly
monster.x = 32 + (Math.random() * (canvas.width - 64));
monster.y = 32 + (Math.random() * (canvas.height - 64));


// #Handle keyboard controls
var keysDown = new Set();

document.addEventListener("keydown", function (e) { keysDown.add(e.keyCode); });

document.addEventListener("keyup", function (e) { keysDown.delete(e.keyCode); });


// #Update game objects
var update = function () 
{
	// Player holding up
	if (keysDown.has(38)) { hero.y -= hero.speed;}
	// Player holding down
	if (keysDown.has(40)) { hero.y += hero.speed;}
	// Player holding left
	if (keysDown.has(37)) { hero.x -= hero.speed;}
	// Player holding right
	if (keysDown.has(39)) { hero.x += hero.speed;}
};


var render = function () 
{   
	//Draw background
	ctx.drawImage(bgImage, 0, 0); 
	//Draw hero
	ctx.drawImage(heroImage, hero.x, hero.y);
	//#draw monster
	ctx.drawImage(monsterImage, monster.x, monster.y); 

};


// The main game loop
var main = function () 
{ 
	update();
	render();

};


// Let's play this game!
window.setInterval(main, 1); // Execute as fast as possible
