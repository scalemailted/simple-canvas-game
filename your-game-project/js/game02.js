// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
var bgReady = false;
var bgImage = new Image();
bgImage.onload = function () { bgReady = true; };
bgImage.src = "images/background.png";

// #Hero image
var heroReady = false;
var heroImage = new Image();
heroImage.onload = function () {
	heroReady = true;
};
heroImage.src = "images/hero.png";

// #Game objects
var hero = { };

//#hero pos
hero.x = canvas.width / 2;
hero.y = canvas.height / 2;


var render = function () 
{ 
	if (bgReady) { ctx.drawImage(bgImage, 0, 0); } 

	if (heroReady) { ctx.drawImage(heroImage, hero.x, hero.y);} //#render hero

};


// The main game loop
var main = function () { render(); };


// Let's play this game!
setInterval(main, 1); // Execute as fast as possible
