// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
var bgReady = false;
var bgImage = new Image();
bgImage.onload = function () { bgReady = true; };
bgImage.src = "images/background.png";

// Hero image
var heroReady = false;
var heroImage = new Image();
heroImage.onload = function () {
	heroReady = true;
};
heroImage.src = "images/hero.png";

// Monster image
var monsterReady = false;
var monsterImage = new Image();
monsterImage.onload = function () {
	monsterReady = true;
};
monsterImage.src = "images/monster.png";

// Game objects
var hero = { speed: 256 /*movement in pixels per second*/ };
var monster = {};
var monstersCaught = 0; //Add counter


// Reset the game when the player catches a monster
var reset = function () {
	hero.x = canvas.width / 2;
	hero.y = canvas.height / 2;

	// Throw the monster somewhere on the screen randomly
	monster.x = 32 + (Math.random() * (canvas.width - 64));
	monster.y = 32 + (Math.random() * (canvas.height - 64));
};


// Handle keyboard controls
var keysDown = {};

addEventListener("keydown", function (e) {
	keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
	delete keysDown[e.keyCode];
}, false);


// Update game objects
var update = function (modifier) 
{
	// Player holding up
	if (38 in keysDown) { hero.y -= hero.speed * modifier;}
	// Player holding down
	if (40 in keysDown) { hero.y += hero.speed * modifier;}
	// Player holding left
	if (37 in keysDown) { hero.x -= hero.speed * modifier;}
	// Player holding right
	if (39 in keysDown) { hero.x += hero.speed * modifier;}

   // #Make sure the objects can't leave the canvas
    if (hero.x < 0) { hero.x = 0 }

    if (hero.x > canvas.width - 32) { hero.x = canvas.width - 32 }

    if (hero.y < 0) { hero.y = 0 }

    if (hero.y > canvas.height - 32) { hero.y = canvas.height - 32 }
    
    if (monster.x < 0) { monster.x = 0 }

    if (monster.x > canvas.width - 32) { monster.x = canvas.width - 32}
    
    if (monster.y < 0) { monster.y = 0 }
    
    if (monster.y > canvas.height - 32) { monster.y = canvas.height - 32 }

	// Are they touching?
	if (
		hero.x <= (monster.x + 32)
		&& monster.x <= (hero.x + 32)
		&& hero.y <= (monster.y + 32)
		&& monster.y <= (hero.y + 32)
	) 
	{
		++monstersCaught; //increment monster counter
		reset();
	}
};

// Draw everything
var render = function () 
{ 
	if (bgReady) { ctx.drawImage(bgImage, 0, 0); } 

	if (heroReady) { ctx.drawImage(heroImage, hero.x, hero.y);}

	if (monsterReady) { ctx.drawImage(monsterImage, monster.x, monster.y);}

	// Score
	ctx.fillStyle = "rgb(250, 250, 250)";
	ctx.font = "24px Helvetica";
	ctx.textAlign = "left";
	ctx.textBaseline = "top";
	ctx.fillText("Goblins caught: " + monstersCaught, 32, 32);

};


// The main game loop
var main = function () 
{ 
	var now = Date.now();
	var delta = now - then;

	update(delta / 1000);
	render();
	then = now;

};


// Let's play this game!
var then = Date.now();
reset();
main();
setInterval(main, 1); // Execute as fast as possible
