// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
var bgReady = false;
var bgImage = new Image();
bgImage.onload = function () { bgReady = true; };
bgImage.src = "images/background.png";


var render = function () { if (bgReady) { ctx.drawImage(bgImage, 0, 0); } };


// The main game loop
var main = function () { render(); };


// Let's play this game!
setInterval(main, 1); // Execute as fast as possible
