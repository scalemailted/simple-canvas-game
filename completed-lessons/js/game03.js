// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
var bgReady = false;
var bgImage = new Image();
bgImage.onload = function () { bgReady = true; };
bgImage.src = "images/background.png";

// Hero image
var heroReady = false;
var heroImage = new Image();
heroImage.onload = function () {
	heroReady = true;
};
heroImage.src = "images/hero.png";

// Game objects
var hero = { speed: 1 /* #movement in pixels per second*/ };

//hero pos
hero.x = canvas.width / 2;
hero.y = canvas.height / 2;


// #Handle keyboard controls
var keysDown = {};

addEventListener("keydown", function (e) { keysDown[e.keyCode] = true; }, false);

addEventListener("keyup", function (e) { delete keysDown[e.keyCode]; }, false);


// #Update game objects
var update = function () 
{
	// Player holding up
	if (38 in keysDown) { hero.y -= hero.speed;}
	// Player holding down
	if (40 in keysDown) { hero.y += hero.speed;}
	// Player holding left
	if (37 in keysDown) { hero.x -= hero.speed;}
	// Player holding right
	if (39 in keysDown) { hero.x += hero.speed;}
};


var render = function () 
{ 
	//Draw the background
	if (bgReady) { ctx.drawImage(bgImage, 0, 0); } 
	//Draw the hero
	if (heroReady) { ctx.drawImage(heroImage, hero.x, hero.y);}

};


// #The main game loop
var main = function () 
{ 
	update();
	render();

};


// #Let's play this game!
setInterval(main, 1); // Execute as fast as possible
